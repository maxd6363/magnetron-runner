# Magnetron Runner


Magnetron Runner est un jeu de plateformes utilisant pour contrôle principal le magnétomètre intégré dans tous les smartphones Android.
Le but est de survivre le plus longtemps possible tout en esquivant les différents obstacles sur la route telle que les lacs et les boules de feu. 
On peut se déplacer vers la droite ou vers la gauche en plus de pouvoir sauter afin d’esquiver les obstacles qui arriveront en face.


# Documentation

Rapport de projet : 

```
./Documentation/Documentation - Magnetron Runner.pdf
```

Fiche de compétences : 

```
./Documentation/Fiche de competences - Magnetron Runner.png
```

Fiche de preuves : 

```
./Documentation/Preuve - Magnetron Runner.pdf
```

APK : 

```
./Installation/MagnetronRunner.apk
```

Bundle :

```
./Installation/MagnetronRunner.aab
```







Maxime POULAIN & Pierre SOUVIGNET
