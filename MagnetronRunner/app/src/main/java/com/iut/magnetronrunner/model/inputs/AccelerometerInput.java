package com.iut.magnetronrunner.model.inputs;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.View;

import com.iut.magnetronrunner.model.utils.ToolBox;

import static android.content.Context.SENSOR_SERVICE;

public class AccelerometerInput extends AbstractInput implements SensorEventListener, ISensorInput {

    private final static float SENSOR_MARGIN = 0.9f;
    private final static float SENSOR_MARGIN_JUMP = 7f;


    protected View view;

    protected float width;
    protected float height;
    protected boolean isSensorRegistered;
    private SensorManager sensorManager;
    protected Sensor accelerometerSensor;

    private float ax;
    private float ay;

    public AccelerometerInput(Context context) {
        super(context);
        ax = 0;
        ay = 0;

        sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        width = ToolBox.getWidthDisplay(applicationContext.getResources());
        height = ToolBox.getHeightDisplay(applicationContext.getResources());

    }


    @Override
    public void update(long elapsed) {
        if (ax > SENSOR_MARGIN) {
            left = true;
            right = false;
        } else if (ax < -SENSOR_MARGIN) {
            right = true;
            left = false;
        }
        else {
            left = false;
            right = false;
        }
        jump = ay > SENSOR_MARGIN_JUMP;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            ax = event.values[0];
            ay = event.values[1];
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    @Override
    public void startSensor() {
        if (!isSensorRegistered) {
            sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_FASTEST);
            isSensorRegistered = true;
        }
    }

    @Override
    public void stopSensor() {
        if (isSensorRegistered) {
            sensorManager.unregisterListener(this, accelerometerSensor);
            isSensorRegistered = false;
        }
    }
}
