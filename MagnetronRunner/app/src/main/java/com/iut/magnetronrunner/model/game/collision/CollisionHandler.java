package com.iut.magnetronrunner.model.game.collision;

import com.iut.magnetronrunner.model.game.gameObjects.GameObject;
import com.iut.magnetronrunner.model.game.manager.GameManager;
import com.iut.magnetronrunner.model.utils.ToolBox;

import java.util.Collection;

public abstract class CollisionHandler {

    private static final int DEFAULT_BELOW_MARGIN_DP = 50;

    protected Collection<GameObject> gameObjects;
    protected GameManager manager;
    protected int heightScreen;
    protected float belowMargin;



    public CollisionHandler(GameManager manager){
        this.manager = manager;
        gameObjects = manager.getGameObjects();
        heightScreen = ToolBox.getHeightDisplay(manager.getApplicationContext().getResources());
        belowMargin = ToolBox.dpToPixel(manager.getApplicationContext().getResources(), DEFAULT_BELOW_MARGIN_DP);

    }


    public abstract void handleCollision();



}
