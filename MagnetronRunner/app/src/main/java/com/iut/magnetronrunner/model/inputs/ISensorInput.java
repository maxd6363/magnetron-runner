package com.iut.magnetronrunner.model.inputs;

public interface ISensorInput {

    void startSensor();


    void stopSensor();
}
