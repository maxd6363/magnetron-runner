package com.iut.magnetronrunner.model.game.gui;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;

import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.SharedPreferencesSettings;
import com.iut.magnetronrunner.model.game.manager.GameManager;
import com.iut.magnetronrunner.model.utils.ToolBox;

import java.util.Locale;

public class SimpleDiedMessage extends DiedMessage {

    private static final int DEFAULT_FONT_SIZE_DP = 60;

    private float posX;
    private float posY;
    private float posYRestart;
    private Paint paint;
    private Paint paintRestart;
    private String message;
    private String restart;

    public SimpleDiedMessage(final boolean activated, final GameManager gameManager) {
        super(activated);
        Resources r = gameManager.getApplicationContext().getResources();

        posX = ToolBox.getWidthDisplay(r) / 2;
        posY = ToolBox.getHeightDisplay(r) / 2;
        posYRestart = posY + ToolBox.dpToPixel(r,80);

        paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(Color.RED);
        paint.setTextSize(ToolBox.dpToPixel(r, DEFAULT_FONT_SIZE_DP));
        paint.setTypeface(ResourcesCompat.getFont(gameManager.getApplicationContext(),R.font.interlude));

        paintRestart = new Paint(paint);
        paintRestart.setTextSize(ToolBox.dpToPixel(r, DEFAULT_FONT_SIZE_DP / 2));


        Configuration configuration = new Configuration();
        configuration.locale = new Locale(new SharedPreferencesSettings(gameManager.getApplicationContext()).getLocale());
        Resources resources = new Resources(gameManager.getApplicationContext().getAssets(),gameManager.getApplicationContext().getResources().getDisplayMetrics(),configuration);



        message = resources.getString(R.string.died_message);
        restart = resources.getString(R.string.died_message_restart);

        gameManager.getGameSurface().setOnClickListener(new View.OnClickListener() {
            private int numberOfClick = 0;
            @Override
            public void onClick(View v) {
                if(gameManager.getDiedMessage().isActivated()){
                    numberOfClick++;
                    if(numberOfClick >= 6){
                        gameManager.resetGame();
                    }
                }
            }
        });

    }


    @Override
    public void draw(Canvas canvas) {
        if (!activated) return;
        canvas.drawText(message, posX, posY, paint);
        canvas.drawText(restart, posX, posYRestart, paintRestart);
    }
}
