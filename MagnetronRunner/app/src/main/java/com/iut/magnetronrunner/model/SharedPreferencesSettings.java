package com.iut.magnetronrunner.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import com.iut.magnetronrunner.R;

import java.util.Locale;

public class SharedPreferencesSettings extends Settings {
    protected SharedPreferences sharedPreferences;


    public SharedPreferencesSettings(Context appContext) {
        super(appContext);
        this.sharedPreferences = appContext.getSharedPreferences(appContext.getString(R.string.sharedPref), Context.MODE_PRIVATE);

    }

    @Override
    public int getValueMusic() {
        return sharedPreferences.getInt(appContext.getString(R.string.musicSharedPref), 50);
    }

    @Override
    public void setValueMusic(int valueMusic) {
        sharedPreferences.edit().putInt(appContext.getString(R.string.musicSharedPref), valueMusic).apply();
    }

    @Override
    public int getValueSound() {
        return sharedPreferences.getInt(appContext.getString(R.string.soundSharedPref), 50);

    }

    @Override
    public void setValueSound(int valueSound) {
        sharedPreferences.edit().putInt(appContext.getString(R.string.soundSharedPref), valueSound).apply();
    }

    @Override
    public int getInput() {
        return sharedPreferences.getInt(appContext.getString(R.string.inputSharedPref),3);
    }

    @Override
    public void setInput(int input) {
        sharedPreferences.edit().putInt(appContext.getString(R.string.inputSharedPref),input).apply();
    }
    @Override
    public String getId() {
        return sharedPreferences.getString(appContext.getString(R.string.idSharedPref),"");
    }

    @Override
    public void setId(String id) {
        sharedPreferences.edit().putString((appContext.getString(R.string.idSharedPref)),id).apply();
    }
    @Override
    public String getName() {
        return sharedPreferences.getString(appContext.getString(R.string.nameSharedPref),"");
    }

    @Override
    public void setName(String name) {
        sharedPreferences.edit().putString((appContext.getString(R.string.nameSharedPref)),name).apply();
    }

    @Override
    public String getLocale() {
        return sharedPreferences.getString(appContext.getString(R.string.localeSharedPref), String.valueOf(Locale.getDefault())).substring(0,2);

    }

    @Override
    public void setLocale(String locale) {
        locale = locale.subSequence(0,2).toString();
        sharedPreferences.edit().putString(appContext.getString(R.string.localeSharedPref),locale).apply();
        Locale localeInstance = new Locale(locale);
        Locale.setDefault(localeInstance);
    }
}
