package com.iut.magnetronrunner.model.game.gui;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.iut.magnetronrunner.model.game.player.Player;
import com.iut.magnetronrunner.model.utils.ToolBox;

public class SimpleGuiScore extends GuiScore {

    private static final int DEFAULT_Y_DP = 40;
    private static final int DEFAULT_FONT_SIZE_DP = 40;
    private static final String FORMAT = "%.1f";
    private static final float SHADOW_RADIUS = 10f;
    private static final float SHADOW_DELTA = 0.5f;
    private static final int SHADOW_COUNT_MAX = 20;
    private static final float SHADOW_DX = 5f;
    private static final float SHADOW_DY = 5f;

    private Player player;
    private float posX;
    private float posY;
    private Paint paint;
    private float shadowRadiusAnimated;
    private int count;
    private boolean increasing;

    public SimpleGuiScore(Player player) {
        this.player = player;
        Resources resources = player.getContext().getResources();
        posX = ToolBox.getWidthDisplay(resources) / 2;
        posY = ToolBox.dpToPixel(resources, DEFAULT_Y_DP);
        paint = new Paint();
        shadowRadiusAnimated = SHADOW_RADIUS;
        count = 0;
        increasing = true;
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(Color.WHITE);
        paint.setShadowLayer(shadowRadiusAnimated, SHADOW_DX, SHADOW_DY, Color.BLACK);
        paint.setTextSize(ToolBox.dpToPixel(resources, DEFAULT_FONT_SIZE_DP));
    }

    @Override
    public void draw(Canvas canvas) {
        shadowEffect();
        canvas.drawText(String.format(player.getContext().getResources().getConfiguration().locale, FORMAT, player.getScore()), posX, posY, paint);
    }


    private void shadowEffect() {
        if (count == SHADOW_COUNT_MAX) {
            increasing = false;
        }
        if (count == 0) {
            increasing = true;
        }

        count = increasing ? count + 1 : count - 1;
        shadowRadiusAnimated = SHADOW_RADIUS +  count * SHADOW_DELTA;
        paint.setShadowLayer(shadowRadiusAnimated, SHADOW_DX, SHADOW_DY, Color.BLACK);
    }
}
