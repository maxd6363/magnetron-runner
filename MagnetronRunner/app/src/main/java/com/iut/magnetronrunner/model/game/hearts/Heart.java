package com.iut.magnetronrunner.model.game.hearts;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.game.gameObjects.GameObject;
import com.iut.magnetronrunner.model.utils.ToolBox;

public class Heart extends GameObject {

    private final static int NUMBER_OF_SPRITE = 3;
    private final static int ANIMATION_DELAY_MAX = 12;
    private final static float DEFAULT_HEART_SCALE = 0.3f;
    private final static float DAMAGE_INFLECTED = 3f;


    public Heart(Context context,int pos) {
        super(context);
        damageInflicted = DAMAGE_INFLECTED;

        resourceSprites.add(R.drawable.animate_heart_1);
        resourceSprites.add(R.drawable.animate_heart_2);
        resourceSprites.add(R.drawable.animate_heart_3);
        sprites = new Bitmap[NUMBER_OF_SPRITE];
        scale = DEFAULT_HEART_SCALE;

        loadSprites();


        Point displaySize = new Point();
        ((WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(displaySize);

        position.setX(ToolBox.dpToPixel(applicationContext.getResources(), 50* pos));
        position.setY(ToolBox.dpToPixel(applicationContext.getResources(), 550));



        processPosition();

    }


    @Override
    public void draw(@NonNull Canvas canvas) {
        canvas.drawBitmap(currentSprite, null, rect, paint);
        super.draw(canvas);
    }


    @Override
    protected void processPosition() {
        try {
            Point displaySize = new Point();
            ((WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(displaySize);
            rect.left = position.getX() - sprites[0].getWidth() / 2;
            rect.top = position.getY() - sprites[0].getHeight() / 2 - ToolBox.dpToPixel(applicationContext.getResources(), 500);
            rect.right = sprites[0].getWidth() + rect.left;
            rect.bottom = sprites[0].getHeight() + rect.top;
            processHitBox();


        } catch (NullPointerException ignored) {
        }
    }




    @Override
    public void update(long elapsedMS) {
        updateAnimation();
    }

    private void updateAnimation() {
        if (animationDelay == 0) {
            indexCurrentSprite = (indexCurrentSprite + 1) % NUMBER_OF_SPRITE;
            currentSprite = sprites[indexCurrentSprite];
        }
        animationDelay = (animationDelay + 1) % ANIMATION_DELAY_MAX;
    }



    @Override
    protected void loadSprites() {


        for (int i = 0; i < NUMBER_OF_SPRITE; i++) {
            loadScaledBitmap(i, resourceSprites.get(i));
        }

        currentSprite = sprites[indexCurrentSprite];
    }


    private void loadScaledBitmap(int index, int resource) {
        Bitmap tmp = BitmapFactory.decodeResource(applicationContext.getResources(), resource);
        sprites[index] = Bitmap.createScaledBitmap(tmp, (int) (scale * tmp.getWidth()), (int) (scale * tmp.getHeight()), false);
    }
}
