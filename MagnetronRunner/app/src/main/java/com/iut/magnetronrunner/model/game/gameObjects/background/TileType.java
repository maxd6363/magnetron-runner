package com.iut.magnetronrunner.model.game.gameObjects.background;

import java.util.Random;

public enum TileType {


    NONE(0),
    GRASS(1),
    LAKE(2),
    LAKE_TO_DOWN(3),
    LAKE_TO_UP(4),
    LAKE_TO_RIGHT(5),
    LAKE_TO_LEFT(6),
    LAKE_LEFT_TO_RIGHT(7),
    LAKE_UP_DOWN(8),
    LAKE_DOWN_RIGHT_UNFILLED(9),
    LAKE_DOWN_LEFT_UNFILLED(10),
    LAKE_UP_RIGHT_UNFILLED(11),
    LAKE_UP_LEFT_UNFILLED(12),
    LAKE_UP_DOWN_LEFT_RIGHT_UNFILLED(13),
    LAKE_DOWN_LEFT_RIGHT_UNFILLED(14),
    LAKE_UP_DOWN_LEFT_UNFILLED(15),
    LAKE_UP_DOWN_RIGHT_UNFILLED(16),
    LAKE_UP_LEFT_RIGHT_UNFILLED(17);


    private final static int PERCENT_LAKE_SPAWN = 35;

    private final int value;
    private Random random;

    TileType(int value) {
        this.value = value;
        random = new Random();
    }

    public int getValue() {
        return value;
    }

    public TileType getRandomTile() {

        int percent = random.nextInt(100);

        if(percent >= 0 && percent < PERCENT_LAKE_SPAWN){
            return LAKE;
        }
        if(percent >= PERCENT_LAKE_SPAWN && percent < 100)
            return GRASS;

        return NONE;

    }

}
