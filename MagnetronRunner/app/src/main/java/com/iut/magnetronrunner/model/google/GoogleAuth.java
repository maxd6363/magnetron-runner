package com.iut.magnetronrunner.model.google;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ListView;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.adapter.ScoreListAdapter;
import com.iut.magnetronrunner.model.Score;
import com.iut.magnetronrunner.model.SharedPreferencesSettings;

import java.util.ArrayList;
import java.util.List;

public class GoogleAuth extends SharedPreferencesSettings {

    private String id;
    private Score[] user;


    public GoogleAuth(Context appContext) {
        super(appContext);
        user = new Score[1];
    }

    public void add_dataBase(final float score) {
        id = getId();
        if (!id.equals("")) {
            Log.d("DATABASE", "GOOGLE IS OK");
            //get user old score
            FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = mFirebaseDatabase.getReference("users");
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {

                        Log.d("DATABASE", " KEY : " + childDataSnapshot.getKey() + " ID : " + id);
                        if (childDataSnapshot.getKey().toString().equals(id.toString())) {
                            Log.d("DATABASE", " KEY  AND ID ARE SAME :  " + id);

                            int rank = Integer.parseInt(childDataSnapshot.child("rank").getValue() + "");
                            float score = Float.parseFloat(childDataSnapshot.child("score").getValue() + "");
                            String name = "" + childDataSnapshot.child("name").getValue();

                            user[0] = new Score(name, score, rank);
                            Log.d("USERINFOS", " USER NAME " + user[0].getName());

                        }
                    }
                    if (user[0] == null || user[0].getScore() < score) {
                        if (user[0] == null)
                            Log.d("DATABASE", "OLD NULL");
                        else
                            Log.d("DATABASE", "OLD : " + user[0].getScore() + " NEW : " + score);
                        Score user = new Score(getName(), score, 0);
                        DatabaseReference mDatabase;
                        mDatabase = FirebaseDatabase.getInstance().getReference();
                        mDatabase.child("users").child(id).setValue(user);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            //compare and insert if better than old
        }
    }

    public ArrayList<Score> get_rank(final ListView lv, final View load, final RotateAnimation rotate) {
        final ArrayList<Score> usersTemp = new ArrayList<>();
        final ArrayList<Score> users = new ArrayList<>();
        Log.d("DATABASE", "will begin");

        FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
        Query databaseReference = mFirebaseDatabase.getReference("users").orderByChild("score").limitToLast(10);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("DATABASE", "Success entering database");
                int i = 0;
                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {

                    int rank = Integer.parseInt(childDataSnapshot.child("rank").getValue() + "");
                    float score = Float.parseFloat(childDataSnapshot.child("score").getValue() + "");
                    String name = "" + childDataSnapshot.child("name").getValue();

                    usersTemp.add(new Score(name, score, rank));
                    i++;
                }
                for (Score us : usersTemp) {
                    us.setRank(i);
                    users.add(0, us);
                    i--;
                }

                final List<Score> users_list = users;

                final ScoreListAdapter adapter = new ScoreListAdapter(appContext, R.layout.adapter_listview_score, users_list);
                lv.setAdapter(adapter);
                load.setVisibility(View.INVISIBLE);
                rotate.cancel();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return users;
    }
}
