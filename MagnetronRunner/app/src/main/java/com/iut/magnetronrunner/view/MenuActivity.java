package com.iut.magnetronrunner.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.Settings;
import com.iut.magnetronrunner.model.SharedPreferencesSettings;
import com.iut.magnetronrunner.model.google.GoogleAuth;

public class MenuActivity extends AppCompatActivity {

    private static final int DEFAULT_ANIMATION_PLAY_DURATION = 2000;
    private static final int DEFAULT_ANIMATION_SCORE_DURATION = 500;
    private static final int DEFAULT_ANIMATION_SETTINGS_DURATION = 500;
    private static final int RC_SIGN_IN = 9001;

    protected Settings settings;
    private FirebaseAuth mAuth;
    private String id;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleAuth googleAuth;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        settings = new SharedPreferencesSettings(this);

        googleAuth = new GoogleAuth(this);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("1089391115151-56ohlmqbsiv48ci4g9iqojnbvir7ie3e.apps.googleusercontent.com")
                .requestEmail()
                .build();

        if (settings.getId().equals("")) {
            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            mAuth = FirebaseAuth.getInstance();
            signIn();
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct != null) {
                String personName = acct.getDisplayName();
                String personGivenName = acct.getGivenName();
                String personFamilyName = acct.getFamilyName();
                String personEmail = acct.getEmail();
                id = acct.getId();
                settings.setId(id);
                settings.setName(personName);
                Log.d("GOOGLEAUTH", personEmail + " : " + personGivenName + " : " + personName + " : " + personFamilyName + " : " + id);
            }
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.d("GOOGLEAUTH", "Google sign in failed" + e);
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("GOOGLEAUTH", "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("GOOGLEAUTH", "signInWithCredential:success");
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("GOOGLEAUTH", "signInWithCredential:failure", task.getException());
                        }
                    }
                });
    }

    public void getStartedClicked(View view) {

        startActivity(GameActivity.class);

    }


    public void scoreClicked(View view) {
        animationActivity(view, ScoreActivity.class, DEFAULT_ANIMATION_SCORE_DURATION, true, false, true);
    }

    public void settingsClicked(View view) {
        animationActivity(view, SettingsActivity.class, DEFAULT_ANIMATION_SETTINGS_DURATION, true, false, true);
    }

    /***
     * Pour set une animation sur n'importe qu'elle view (tourne sur elle même)
     * @param view la vue
     * @param name la Class qui est en relation, si il n'y a aucun lancement d'activité on peut mettre n'importe quoi ça ne changera pas
     * @param duration la duree en millisecondes
     * @param isAnimate animation ?
     * @param isInfinite infini avec avec option 'reverse' => reviens sur lui meme avant de refaire l'animation
     * @param isLaunching Est-ce que ça lance l'activité lié à la classe précisé juste avant dans 'name'
     */
    public void animationActivity(View view, final Class name, long duration, boolean isAnimate, boolean isInfinite, boolean isLaunching) {
        if (isAnimate) {
            final RotateAnimation rotate = new RotateAnimation(
                    0, 360,
                    Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF, 0.5f
            );
            if (isInfinite) {
                rotate.setRepeatCount(Animation.INFINITE);
                rotate.setRepeatMode(Animation.REVERSE);
            }
            rotate.setDuration(duration);
            if (isLaunching) {
                rotate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        startActivity(name);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            view.startAnimation(rotate);
        } else
            startActivity(name);
    }

    /**
     * Lancer n'importe quelle activité a partir de sa classe (pour eviter la redondance)
     *
     * @param name nom de l'activité à lancer
     */
    public void startActivity(Class name) {
        Intent activity = new Intent(this, name);
        startActivity(activity);
    }
}
