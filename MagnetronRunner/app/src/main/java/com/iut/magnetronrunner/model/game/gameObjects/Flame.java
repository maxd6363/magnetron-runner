package com.iut.magnetronrunner.model.game.gameObjects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.game.factory.SimpleFactory;
import com.iut.magnetronrunner.model.utils.ToolBox;

import java.util.Random;


public class Flame extends GameObject {

    private final static int NUMBER_OF_SPRITE = 4;
    private final static int ANIMATION_DELAY_MAX = 4;
    private final static float DEFAULT_RUNNER_SCALE = 1f;
    private final static float DAMAGE_INFLECTED = 1f;
    private final static int RANDOM_SPEED_BOUND = 2;
    private final static int RANDOM_SPEED_MARGIN = 2;

    private int fallSpeed;

    public Flame(Context context, int x) {
        super(context);
        factory = new SimpleFactory(context);
        damageInflicted = DAMAGE_INFLECTED;

        resourceSprites.add(R.drawable.fireball_1);
        resourceSprites.add(R.drawable.fireball_2);
        resourceSprites.add(R.drawable.fireball_3);
        resourceSprites.add(R.drawable.fireball_4);

        sprites = new Bitmap[NUMBER_OF_SPRITE];
        scale = DEFAULT_RUNNER_SCALE;

        loadSprites();

        fallSpeed = new Random().nextInt(RANDOM_SPEED_BOUND) + RANDOM_SPEED_MARGIN;

        Point displaySize = new Point();
        ((WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(displaySize);

        position.setX(ToolBox.dpToPixel(applicationContext.getResources(), x));
        position.setY(ToolBox.dpToPixel(applicationContext.getResources(), 0));
        processPosition();

    }


    @Override
    public void draw(@NonNull Canvas canvas) {
        canvas.drawBitmap(currentSprite, null, rect, paint);
        super.draw(canvas);
    }

    private void fall() {
        int delta = ToolBox.dpToPixel(applicationContext.getResources(), ToolBox.dpToPixel(applicationContext.getResources(), fallSpeed));
        moveY(delta);
    }

    @Override
    protected void processPosition() {
        try {
            Point displaySize = new Point();
            ((WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(displaySize);
            rect.left = position.getX() - sprites[0].getWidth() / 2;
            rect.top = position.getY() - sprites[0].getHeight() / 2 - ToolBox.dpToPixel(applicationContext.getResources(), 500);
            rect.right = sprites[0].getWidth() + rect.left;
            rect.bottom = sprites[0].getHeight() + rect.top;
            processHitBox();


        } catch (NullPointerException ignored) {
        }
    }


    @Override
    public void update(long elapsedMS) {
        updateAnimation();
        updatePosition();
    }

    private void updateAnimation() {
        if (animationDelay == 0) {
            indexCurrentSprite = (indexCurrentSprite + 1) % NUMBER_OF_SPRITE;
            currentSprite = sprites[indexCurrentSprite];
        }
        animationDelay = (animationDelay + 1) % ANIMATION_DELAY_MAX;
    }

    private void updatePosition() {
        fall();
        processHitBox();
    }

    @Override
    protected void loadSprites() {
        for (int i = 0; i < NUMBER_OF_SPRITE; i++) {
            loadScaledBitmap(i, resourceSprites.get(i));
        }
        currentSprite = sprites[indexCurrentSprite];
    }


    private void loadScaledBitmap(int index, int resource) {
        Bitmap tmp = factory.loadBitmap(applicationContext.getResources(), resource);
        sprites[index] = Bitmap.createScaledBitmap(tmp, (int) (scale * tmp.getWidth()), (int) (scale * tmp.getHeight()), false);
    }

    public int getFallSpeed() {
        return fallSpeed;
    }

    public void setFallSpeed(int fallSpeed) {
        this.fallSpeed = fallSpeed;
    }
}
