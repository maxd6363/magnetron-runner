package com.iut.magnetronrunner.model.game.gameObjects;

import android.graphics.Canvas;

public interface IDrawable {
    void draw(Canvas canvas);
}
