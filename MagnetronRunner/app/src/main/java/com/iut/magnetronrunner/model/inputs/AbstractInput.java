package com.iut.magnetronrunner.model.inputs;

import android.content.Context;

import androidx.annotation.NonNull;

import com.iut.magnetronrunner.model.game.gameObjects.IUpdatable;

/**
 * la vue appelera update sur le player, qui viendra chercher les valeurs "jump", "left" et "right"
 * à chaque frame pour faire bouger le personnage à l'écran
 */
public abstract class AbstractInput implements IUpdatable {


    protected boolean jump;
    protected boolean left;
    protected boolean right;
    protected Context applicationContext;

    public AbstractInput(Context context) {
        jump = false;
        left = false;
        right = false;
        applicationContext = context;
    }


    @NonNull
    @Override
    public String toString() {
        return "AbstractInput";
    }


    @Override
    public abstract void update(long elapsed);


    public boolean isJumping() {
        return jump;
    }

    public boolean isLeft() {
        return left;
    }

    public boolean isRight() {
        return right;
    }


}
