package com.iut.magnetronrunner.view;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.Settings;
import com.iut.magnetronrunner.model.SharedPreferencesSettings;

import java.util.Locale;

public class SettingsActivity extends AppCompatActivity {

    private final static int MAGNET_ID = 1;
    private final static int TOUCH_ID = 2;
    private final static int ACCEL_ID = 3;
    private final static float MAX_ALPHA = 1.0f;
    private final static float MIDDLE_ALPHA = 0.4f;
    private final static String LANG_ENGLISH = "en";
    private final static String LANG_FRENCH = "fr";

    private TextView textViewMusic;
    private TextView textViewSound;

    private SeekBar seekBarMusic;
    private SeekBar seekBarSound;

    private View magnetImage;
    private View handImage;
    private View phoneImage;

    private View icon_lang;

    private EditText name;
    private Button change_name;

    private Settings settings;

    @SuppressLint("SetTextI18n")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        settings = new SharedPreferencesSettings(getApplicationContext());

        setContentView(R.layout.activity_settings);

        seekBarMusic = findViewById(R.id.seekBarMusic);
        seekBarSound = findViewById(R.id.seekBarSound);
        textViewMusic = findViewById(R.id.valueMusic);
        textViewSound = findViewById(R.id.valueSound);

        magnetImage = findViewById(R.id.magnet);
        handImage = findViewById(R.id.hand);
        phoneImage = findViewById(R.id.phone);

        icon_lang = findViewById(R.id.lang_icon);

        name = findViewById(R.id.name);
        change_name = findViewById(R.id.change_name);

        name.setText(settings.getName());
        textViewMusic.setText(String.valueOf(settings.getValueMusic()));
        seekBarMusic.setProgress(settings.getValueMusic());
        textViewSound.setText(String.valueOf(settings.getValueSound()));
        seekBarSound.setProgress(settings.getValueSound());
        setLangIcon();
        processAlphaIcon(settings.getInput());

        change_name.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                settings.setName(name.getText().toString());
                finish();
            }
        });

        magnetImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), R.string.magnet_message, Toast.LENGTH_SHORT).show();
                settings.setInput(MAGNET_ID);
                processAlphaIcon(MAGNET_ID);
            }
        });
        handImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), R.string.hand_message, Toast.LENGTH_SHORT).show();
                settings.setInput(TOUCH_ID);
                processAlphaIcon(TOUCH_ID);

            }
        });
        phoneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), R.string.phone_message, Toast.LENGTH_SHORT).show();
                settings.setInput(ACCEL_ID);
                processAlphaIcon(ACCEL_ID);
            }
        });

        seekBarMusic.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @SuppressLint("SetTextI18n")
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                settings.setValueMusic(progress);
                textViewMusic.setText(String.valueOf(settings.getValueMusic()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekBarSound.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                settings.setValueSound(progress);
                textViewSound.setText(String.valueOf(settings.getValueSound()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }

    public void changeLang(View view) {
        switch (settings.getLocale()) {
            case LANG_ENGLISH:
                changeLangByLanguageName(LANG_FRENCH);
                break;
            case LANG_FRENCH:
                changeLangByLanguageName(LANG_ENGLISH);
        }
    }

    private void setLangIcon() {
        switch (settings.getLocale()) {
            case LANG_ENGLISH:
                icon_lang.setBackgroundResource(R.drawable.ic_flag_en);
                break;
            case LANG_FRENCH:
                icon_lang.setBackgroundResource(R.drawable.ic_flag_fr);
                break;
        }
    }

    private void changeLangByLanguageName(String lang) {
        settings.setLocale(lang);
        Configuration conf = new Configuration();
        conf.locale = new Locale(settings.getLocale());
        getBaseContext().getResources().updateConfiguration(conf, getBaseContext().getResources().getDisplayMetrics());
        reCreate();
    }

    public void reCreate() {
        super.onDestroy();
        onCreate(new Bundle());
    }

    private void processAlphaIcon(int idToSet) {
        magnetImage.setAlpha(MIDDLE_ALPHA);
        handImage.setAlpha(MIDDLE_ALPHA);
        phoneImage.setAlpha(MIDDLE_ALPHA);

        switch (idToSet) {
            case MAGNET_ID:
                magnetImage.setAlpha(MAX_ALPHA);
                break;
            case TOUCH_ID:
                handImage.setAlpha(MAX_ALPHA);
                break;
            case ACCEL_ID:
                phoneImage.setAlpha(MAX_ALPHA);
                break;
        }
    }

}
