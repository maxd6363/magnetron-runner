package com.iut.magnetronrunner.model.game.generator;

import com.iut.magnetronrunner.model.game.factory.Factory;
import com.iut.magnetronrunner.model.game.gameObjects.IUpdatable;
import com.iut.magnetronrunner.model.game.manager.GameManager;

public abstract class GameObjectGenerator implements IUpdatable {

    protected GameManager gameManager;
    protected Factory factory;


    public GameObjectGenerator(GameManager gameManager){
        this.gameManager = gameManager;
    }

    protected abstract void generateFlame();




}
