package com.iut.magnetronrunner.model.game.player;

import com.iut.magnetronrunner.model.Settings;
import com.iut.magnetronrunner.model.SharedPreferencesSettings;
import com.iut.magnetronrunner.model.game.gameObjects.IDrawable;
import com.iut.magnetronrunner.model.game.gameObjects.Runner;
import com.iut.magnetronrunner.model.game.gui.SimpleGuiScore;
import com.iut.magnetronrunner.model.game.manager.GameManager;
import com.iut.magnetronrunner.model.inputs.AbstractInput;
import com.iut.magnetronrunner.model.inputs.AccelerometerInput;
import com.iut.magnetronrunner.model.inputs.MagnetInput;
import com.iut.magnetronrunner.model.inputs.TouchInput;

public class SimplePlayer extends Player {

    private final static float MOVE_GAME_OBJECT = 0.8f;

    private boolean jump;
    private boolean left;
    private boolean right;
    private TouchInput touchInput;
    private MagnetInput magnetInput;
    private AccelerometerInput accelerometerInput;
    private Settings settings;
    private IDrawable guiScore;
    private boolean scoreCount;

    public SimplePlayer(GameManager gameManager) {
        super(gameManager);
        settings = new SharedPreferencesSettings(context);
        switch (settings.getInput()) {
            case 1:
                magnetInput = new MagnetInput(context);
                inputs.add(magnetInput);
                break;
            case 2:
                touchInput = new TouchInput(context, gameSurface);
                inputs.add(touchInput);
                break;
            case 3:
                accelerometerInput = new AccelerometerInput(context);
                inputs.add(accelerometerInput);
        }

        jump = false;
        left = false;
        right = false;
        score = 0f;
        guiScore = new SimpleGuiScore(this);
        gameManager.addGuiElement(guiScore);

        startSensors();
        startScore();
    }


    @Override
    public void update(long elapsed) {
        if (controlledObject == null) return;
        jump = false;
        left = false;
        right = false;
        float value = MOVE_GAME_OBJECT;

        for (AbstractInput input : inputs) {
            input.update(elapsed);
            if (input.isJumping())
                jump = true;
            if (input.isLeft())
                left = true;
            if (input.isRight())
                right = true;
        }

        if (left) {
            controlledObject.moveX(elapsed * -value);
        }
        if (right) {
            controlledObject.moveX(elapsed * value);
        }
        if (jump) {
            controlledObject.setHitEnabled(false);
        }

        if (scoreCount && elapsed / 1000f < 1)
            score += elapsed / 1000f;
    }


    @Override
    public void controlledObjectTookDamage(float damage) {
        if (controlledObject == null) return;
        ((Runner) controlledObject).takeDamage(damage);
        if (controlledObject.getHealth() <= 0) {
            gameManager.endGame();
        }

    }

    @Override
    public void stopScore() {
        scoreCount = false;
    }

    @Override
    public void startScore() {
        scoreCount = true;
    }


    public void stopSensors() {
        if (magnetInput != null)
            magnetInput.stopSensor();
        if (accelerometerInput != null)
            accelerometerInput.stopSensor();
    }

    public void startSensors() {
        if (magnetInput != null)
            magnetInput.startSensor();
        if (accelerometerInput != null)
            accelerometerInput.startSensor();
    }


}
