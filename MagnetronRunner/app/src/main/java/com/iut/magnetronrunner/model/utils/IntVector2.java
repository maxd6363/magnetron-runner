package com.iut.magnetronrunner.model.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class IntVector2 {
    private int x;
    private int y;

    public IntVector2(){
        this(0,0);
    }

    public IntVector2(IntVector2 intVector2){ this(intVector2.x, intVector2.y);}

    public IntVector2(int x, int y){
        this.x = x;
        this.y = y;
    }


    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }


    @NonNull
    @Override
    public String toString() {
        return "X : " + x + " | Y : " + y;
    }

    @NonNull
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        return new IntVector2(x,y);
    }


    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj==null) return false;
        if(obj==this) return true;
        if(obj.getClass() != getClass()) return false;
        IntVector2 other = (IntVector2) obj;
        return x == other.x && y==other.y;
    }

    @Override
    public int hashCode() {
        return 4568745 * x * y;
    }


}

