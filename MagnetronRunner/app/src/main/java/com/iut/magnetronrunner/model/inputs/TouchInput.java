package com.iut.magnetronrunner.model.inputs;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;

import com.iut.magnetronrunner.model.utils.ToolBox;

public class TouchInput extends AbstractInput {

    private final static int MAX_MULTITOUCH = 11;


    protected View view;
    protected int numberOfTouch;
    protected float[] touchX;
    protected float[] touchY;
    protected float width;
    protected float height;

    public TouchInput(Context context, final View view) {
        super(context);
        this.view = view;
        numberOfTouch = 0;
        touchX = new float[MAX_MULTITOUCH];
        touchY = new float[MAX_MULTITOUCH];


        for (int i = 0; i < MAX_MULTITOUCH; i++) {
            touchX[i] = -1;
            touchY[i] = -1;
        }

        width = ToolBox.getWidthDisplay(applicationContext.getResources());
        height = ToolBox.getHeightDisplay(applicationContext.getResources());

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.performClick();
                int index = event.getActionIndex();
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_POINTER_DOWN:
                        touchX[index] = event.getX(index);
                        touchY[index] = event.getY(index);
                        numberOfTouch--;
                        return true;


                    case MotionEvent.ACTION_UP:
                        for (int i = 0; i < MAX_MULTITOUCH; i++) {
                            touchX[i]=-1;
                            touchY[i]=-1;
                        }
                        numberOfTouch = 0;
                        return true;

                    case MotionEvent.ACTION_POINTER_UP:
                        touchX[index] = -1;
                        touchY[index] = -1;
                        numberOfTouch++;
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        touchX[index] = event.getX(index);
                        touchY[index] = event.getY(index);
                        return true;


                }
                return false;
            }
        });


    }

    private StringBuilder str = new StringBuilder();

    @Override
    public void update(long elapsed) {
        str.append("\n");
        for (int i = 0; i < MAX_MULTITOUCH; i++) {
            str.append(touchX[i]).append(" : ").append(touchY[i]).append("               ").append(i).append("\n");

            if (touchX[i] == -1) continue;

            if (touchX[i] < width / 2) {
                left = true;
            }
            if (touchX[i] > width / 2) {
                right = true;
            }

            if(right && left){
                jump = true;
            }
            else{
                jump = false;
            }
        }


        str.setLength(0);
        if (numberOfTouch == 0) {
            left = false;
            right = false;
            jump = false;
        }
    }
}
