package com.iut.magnetronrunner.model.game.gameObjects.background;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.game.factory.SimpleFactory;
import com.iut.magnetronrunner.model.utils.ToolBox;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class DynamicNaturalBackground extends NaturalBackground {

    private final static int BACKGROUND_WIDTH = 7;
    private final static int BACKGROUND_HEIGHT = 30;
    private final static int NUMBER_OF_SPRITE = 18;
    private final static int SPEED = 8;
    private final static float LAKE_HIT_BOX_RATIO_WIDTH = 0.5f;
    private final static float LAKE_HIT_BOX_RATIO_HEIGHT = 0.4f;
    private final static float DAMAGE_INFLECTED = 0.5f;


    private TileType[][] tiles;
    private TileType generator;
    private int displayHeight;
    private int tileWidth;
    private int tileHeight;
    private Rect rectForDraw;
    private int deltaY;
    private int deltaX;
    private Collection<Rect> hitBoxes;
    private Collection<Rect> hitBoxesToDelete;

    public DynamicNaturalBackground(Context context) {
        super(context);
        factory = new SimpleFactory(context);
        tiles = new TileType[BACKGROUND_WIDTH][BACKGROUND_HEIGHT];
        generator = TileType.NONE;
        displayHeight = ToolBox.getHeightDisplay(context.getResources());
        tileWidth = displayHeight / BACKGROUND_WIDTH;
        tileHeight = tileWidth;

        deltaY = -displayHeight;
        deltaX = -2 * tileWidth;

        rectForDraw = new Rect();

        hitBoxes = new ArrayList<>();
        hitBoxesToDelete = new ArrayList<>();

        resourceSprites.add(R.drawable.tile_none);
        resourceSprites.add(R.drawable.tile_grass);
        resourceSprites.add(R.drawable.tile_lake);
        resourceSprites.add(R.drawable.tile_lake_to_down);
        resourceSprites.add(R.drawable.tile_lake_to_up);
        resourceSprites.add(R.drawable.tile_lake_to_right);
        resourceSprites.add(R.drawable.tile_lake_to_left);
        resourceSprites.add(R.drawable.tile_lake_to_left_right);
        resourceSprites.add(R.drawable.tile_lake_to_up_down);
        resourceSprites.add(R.drawable.tile_lake_to_down_right_unfilled);
        resourceSprites.add(R.drawable.tile_lake_to_down_left_unfilled);
        resourceSprites.add(R.drawable.tile_lake_to_up_right_unfilled);
        resourceSprites.add(R.drawable.tile_lake_to_up_left_unfilled);
        resourceSprites.add(R.drawable.tile_lake_up_down_left_right_unfilled);
        resourceSprites.add(R.drawable.tile_lake_down_left_right_unfilled);
        resourceSprites.add(R.drawable.tile_lake_up_down_left_unfilled);
        resourceSprites.add(R.drawable.tile_lake_up_down_right_unfilled);
        resourceSprites.add(R.drawable.tile_lake_up_left_right_unfilled);

        sprites = new Bitmap[NUMBER_OF_SPRITE];

        loadSprites();
        randomBackground();

    }

    @Override
    public void update(long elapsedMS) {
        processPosition();
    }


    private void randomBackground() {
        for (int i = 0; i < BACKGROUND_WIDTH; i++) {
            for (int j = 0; j < BACKGROUND_HEIGHT; j++) {
                if (i == 0) {
                    tiles[0][j] = TileType.GRASS;
                    continue;
                }
                if (i == BACKGROUND_WIDTH - 1) {
                    tiles[BACKGROUND_WIDTH - 1][j] = TileType.GRASS;
                    continue;
                }
                tiles[i][j] = generator.getRandomTile();
                if (tiles[i][j] == TileType.LAKE) {
                    hitBoxes.add(getHitBox(i, j));
                }
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        for (int i = 0; i < BACKGROUND_WIDTH; i++) {
            for (int j = 0; j < BACKGROUND_HEIGHT; j++) {
                drawTile(canvas, i, j);
            }
        }
        if (COLLISION_DEBUG) {
            for (Rect r : hitBoxes) {
                canvas.drawRoundRect(new RectF(r), 1, 1, new Paint());
            }
        }
    }

    @Override
    protected void processPosition() {
        fall();
    }

    private void hitBoxesFall() {
        for (Rect r : hitBoxes) {
            r.top += SPEED;
            r.bottom += SPEED;
        }
    }

    private void fall() {
        moveY(SPEED);
        hitBoxesFall();
        if (deltaY >= -displayHeight + tileHeight) {
            deltaY = -displayHeight;
            moveTiles();
        }

    }

    private void moveTiles() {
        for (int i = 0; i < BACKGROUND_WIDTH - 1; i++) {
            for (int j = BACKGROUND_HEIGHT - 1; j > 0; j--) {
                tiles[i][j] = tiles[i][j - 1];
            }
        }
        for (int i = 0; i < BACKGROUND_WIDTH; i++) {
            tiles[i][0] = generator.getRandomTile();
            if (tiles[i][0] == TileType.LAKE) {
                hitBoxes.add(getHitBox(i, 0));


            }

        }
    }

    @Override
    protected void loadSprites() {
        for (int i = 0; i < NUMBER_OF_SPRITE; i++) {
            loadScaledBitmap(i, resourceSprites.get(i));
        }
        currentSprite = sprites[indexCurrentSprite];
    }


    private void drawTile(Canvas canvas, int x, int y) {
        rectForDraw.left = x * tileWidth + deltaX;
        rectForDraw.top = y * tileHeight + deltaY;
        rectForDraw.right = (x + 1) * tileWidth + deltaX;
        rectForDraw.bottom = (y + 1) * tileHeight + deltaY;

        Bitmap toDraw;
        if (tiles[x][y] == TileType.LAKE) {
            TileType visualType = TileType.LAKE;
            if (isMaskCorrect(x, y, 0B00010000))
                visualType = TileType.LAKE_TO_DOWN;
            if (isMaskCorrect(x, y, 0B00001000))
                visualType = TileType.LAKE_TO_UP;
            if (isMaskCorrect(x, y, 0B01000000))
                visualType = TileType.LAKE_TO_RIGHT;
            if (isMaskCorrect(x, y, 0B00000010))
                visualType = TileType.LAKE_TO_LEFT;
            if (isMaskCorrect(x, y, 0B01000010))
                visualType = TileType.LAKE_LEFT_TO_RIGHT;
            if (isMaskCorrect(x, y, 0B00011000))
                visualType = TileType.LAKE_UP_DOWN;
            if (isMaskCorrect(x, y, 0B01010000))
                visualType = TileType.LAKE_DOWN_RIGHT_UNFILLED;
            if (isMaskCorrect(x, y, 0B00010010))
                visualType = TileType.LAKE_DOWN_LEFT_UNFILLED;
            if (isMaskCorrect(x, y, 0B01001000))
                visualType = TileType.LAKE_UP_RIGHT_UNFILLED;
            if (isMaskCorrect(x, y, 0B00001010))
                visualType = TileType.LAKE_UP_LEFT_UNFILLED;
            if (isMaskCorrect(x, y, 0B01010010))
                visualType = TileType.LAKE_DOWN_LEFT_RIGHT_UNFILLED;
            if (isMaskCorrect(x, y, 0B00011010))
                visualType = TileType.LAKE_UP_DOWN_LEFT_UNFILLED;
            if (isMaskCorrect(x, y, 0B01011000))
                visualType = TileType.LAKE_UP_DOWN_RIGHT_UNFILLED;
            if (isMaskCorrect(x, y, 0B01001010))
                visualType = TileType.LAKE_UP_LEFT_RIGHT_UNFILLED;
            if (isMaskCorrect(x, y, 0B01011010))
                visualType = TileType.LAKE_UP_DOWN_LEFT_RIGHT_UNFILLED;

            toDraw = sprites[visualType.getValue()];
        } else {
            toDraw = sprites[tiles[x][y].getValue()];
        }

        canvas.drawBitmap(toDraw, null, rectForDraw, paint);
    }


    @Override
    public void moveY(float delta) {
        deltaY += delta;
        for (Rect r : hitBoxes) {
            if (r.bottom > displayHeight)
                hitBoxesToDelete.add(r);
        }
        for (Rect r : hitBoxesToDelete) {
            hitBoxes.remove(r);
        }
        hitBoxesToDelete.clear();
    }


    private void loadScaledBitmap(int index, int resource) {
        Bitmap tmp = factory.loadBitmap(applicationContext.getResources(), resource);
        sprites[index] = Bitmap.createScaledBitmap(tmp, (int) (scale * tmp.getWidth()), (int) (scale * tmp.getHeight()), false);
    }


    /**
     * get bit out of a integer and check if the mask is corresponding to the lake
     *
     * @param x    position à check en X
     * @param y    position à check en Y
     * @param mask must be 8 bits long
     * @return answer
     */
    private boolean isMaskCorrect(int x, int y, int mask) {
        int countBitMask = 0;
        if (tiles[x][y] != TileType.LAKE) return false;

        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (i == 0 && j == 0) continue; // center position
                if (ToolBox.getBit(mask, countBitMask)) {
                    if (x + i < 0 || x + i >= BACKGROUND_WIDTH || y + j < 0 || y + j >= BACKGROUND_HEIGHT) {
                        countBitMask++;
                        continue;
                    }

                    if (tiles[x + i][y + j] != TileType.LAKE)
                        return false;
                }


                countBitMask++;
            }
        }
        return true;
    }


    public Collection<Rect> getHitBoxes() {
        return Collections.unmodifiableCollection(hitBoxes);
    }


    private Rect getHitBox(int x, int y) {
        Rect hit = new Rect();
        hit.left = (int) (x * tileWidth + deltaX + ((1 - LAKE_HIT_BOX_RATIO_WIDTH) / 2) * tileWidth);
        hit.top = (int) (y * tileHeight + deltaY + ((1 - LAKE_HIT_BOX_RATIO_HEIGHT) / 2) * tileHeight);
        hit.right = (int) ((x + 1) * tileWidth + deltaX - ((1 - LAKE_HIT_BOX_RATIO_WIDTH) / 2) * tileWidth);
        hit.bottom = (int) ((y + 1) * tileHeight + deltaY - ((1 - LAKE_HIT_BOX_RATIO_HEIGHT) / 2) * tileHeight);
        return hit;
    }

    @Override
    public float getDamageInflicted() {
        return DAMAGE_INFLECTED;
    }
}
