package com.iut.magnetronrunner.model.game.hearts;

import com.iut.magnetronrunner.model.game.factory.Factory;
import com.iut.magnetronrunner.model.game.gameObjects.GameObject;
import com.iut.magnetronrunner.model.game.gameObjects.IUpdatable;
import com.iut.magnetronrunner.model.game.manager.GameManager;

import java.util.ArrayList;
import java.util.Collection;

public abstract class HeartManager implements IUpdatable {

    protected final static int INITIAL_COLLECTION_SIZE = 5;

    protected GameManager gameManager;
    protected Factory factory;
    protected Collection<GameObject> hearts;

    public HeartManager(GameManager gameManager){
        this.gameManager = gameManager;
        hearts = new ArrayList<>(INITIAL_COLLECTION_SIZE);
    }

    public abstract void setNumberOfHearts(int number);

    protected abstract void generateHeart(int pos);




}
