package com.iut.magnetronrunner.model.game.player;

import android.content.Context;
import android.view.View;

import com.iut.magnetronrunner.model.game.gameObjects.GameObject;
import com.iut.magnetronrunner.model.game.gameObjects.IUpdatable;
import com.iut.magnetronrunner.model.game.manager.GameManager;
import com.iut.magnetronrunner.model.inputs.AbstractInput;

import java.util.ArrayList;
import java.util.List;

public abstract class Player implements IUpdatable {


    protected GameObject controlledObject;

    protected  GameManager gameManager;
    protected Context context;
    protected View gameSurface;
    protected List<AbstractInput> inputs;
    protected float score;

    public Player(GameManager gameManager) {
        this.gameManager = gameManager;
        controlledObject = null;
        context = gameManager.getApplicationContext();
        gameSurface = gameManager.getGameSurface();
        inputs = new ArrayList<>();
    }


    public GameObject getControlledObject() {
        return controlledObject;
    }

    public void setControlledObject(GameObject controlledObject) {
        if (this.controlledObject != controlledObject)
            this.controlledObject = controlledObject;
    }

    public abstract void controlledObjectTookDamage(float damage);

    public float getScore() {
        return score;
    }

    public Context getContext() {
        return context;
    }

    public abstract void stopScore();

    public abstract void startScore();
}
