package com.iut.magnetronrunner.model.game.gameObjects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.game.factory.SimpleFactory;
import com.iut.magnetronrunner.model.utils.ToolBox;

/**
 * Classe représentant le joueur devant sauter ou esquiver les obstacles
 */
public class Runner extends GameObject {


    public final static int OPACITY_DEAD = 100;

    private final static int NUMBER_OF_SPRITE = 6;
    private final static float ANIMATION_DELAY_MAX = 0.06f;
    private final static float ANIMATION_DELAY_DAMAGE = ANIMATION_DELAY_MAX * 3;
    private final static float ANIMATION_DELAY_JUMP = ANIMATION_DELAY_MAX * 5;
    private final static float DEFAULT_RUNNER_SCALE = 0.5f;
    private final static float DEFAULT_RUNNER_HEALTH = 3f;
    private final static float DEFAULT_RUNNER_RECOVERY = 3f;
    private final static float DEFAULT_RUNNER_JUMP_TIME = 1f;
    private final static float DEFAULT_RUNNER_JUMP_TIME_COOL = DEFAULT_RUNNER_JUMP_TIME * 1.2f;


    private float recoveryTime;
    private float jumpTime;
    private float jumpTimeCool;
    private Bitmap damageSprite;
    private Bitmap jumpSprite;
    private boolean ghostMode;

    public Runner(Context context) {
        super(context);
        recoveryTime = DEFAULT_RUNNER_RECOVERY;
        jumpTime = DEFAULT_RUNNER_JUMP_TIME;
        jumpTimeCool = DEFAULT_RUNNER_JUMP_TIME_COOL;
        factory = new SimpleFactory(context);
        health = DEFAULT_RUNNER_HEALTH;

        resourceSprites.add(R.drawable.runner2_loop1);
        resourceSprites.add(R.drawable.runner2_loop2);
        resourceSprites.add(R.drawable.runner2_loop3);
        resourceSprites.add(R.drawable.runner2_loop4);
        resourceSprites.add(R.drawable.runner2_loop5);
        resourceSprites.add(R.drawable.runner2_loop6);

        sprites = new Bitmap[NUMBER_OF_SPRITE];
        scale = DEFAULT_RUNNER_SCALE;

        loadSprites();


        Point displaySize = new Point();
        ((WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(displaySize);

        position.setX(displaySize.x / 2);
        position.setY(displaySize.y);
        processPosition();
        ghostMode = false;
    }


    @Override
    public void draw(@NonNull Canvas canvas) {
        if(!ghostMode){
            paint.setAlpha(recoveryTime > 0 ? OPACITY_DEAD : 255);
        }
        canvas.drawBitmap(currentSprite, null, rect, paint);
        super.draw(canvas);
    }


    @Override
    protected void processPosition() {
        try {
            Point displaySize = new Point();
            ((WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(displaySize);
            rect.left = position.getX() - sprites[0].getWidth() / 2;
            rect.top = position.getY() - sprites[0].getHeight() / 2 - ToolBox.dpToPixel(applicationContext.getResources(), 150);
            rect.right = sprites[0].getWidth() + rect.left;
            rect.bottom = sprites[0].getHeight() + rect.top;
            processHitBox();
        } catch (NullPointerException ignored) {
        }
    }



    public void takeDamage(float damage) {
        if(recoveryTime >= 0) return;
        currentSprite = damageSprite;
        animationDelay = ANIMATION_DELAY_DAMAGE;
        health -= damage;
        recoveryTime = DEFAULT_RUNNER_RECOVERY;
    }


    @Override
    public void update(long elapsedMS) {
        updateAnimation(elapsedMS);
        if(recoveryTime >= 0)
            recoveryTime -= elapsedMS / 1000f;
        if(jumpTime >= 0)
            jumpTime -= elapsedMS / 1000f;
        hitEnabled = !(jumpTime > 0);

        if(jumpTimeCool >= 0)
            jumpTimeCool -= elapsedMS / 1000f;
    }


    @Override
    public void moveX(float delta) {
        if (rect.left + delta < 0) return;
        if (rect.right + delta > ToolBox.getWidthDisplay(applicationContext.getResources())) return;
        processHitBox();
        super.moveX(delta);
    }

    private void updateAnimation(long elapsedMS) {
        if (animationDelay <= 0) {
            animationDelay = ANIMATION_DELAY_MAX;
            indexCurrentSprite = (indexCurrentSprite + 1) % NUMBER_OF_SPRITE;
            currentSprite = sprites[indexCurrentSprite];
        }
        animationDelay -= elapsedMS / 1000f;

    }

    @Override
    protected void loadSprites() {
        for (int i = 0; i < NUMBER_OF_SPRITE; i++) {
            loadScaledBitmap(i, resourceSprites.get(i));
        }
        damageSprite = factory.loadBitmap(applicationContext.getResources(),R.drawable.runner2_damage);
        jumpSprite = factory.loadBitmap(applicationContext.getResources(), R.drawable.runner2_jump);
        currentSprite = sprites[indexCurrentSprite];
    }



    private void loadScaledBitmap(int index, int resource) {
        Bitmap tmp = factory.loadBitmap(applicationContext.getResources(),resource);
        sprites[index] = Bitmap.createScaledBitmap(tmp, (int) (scale * tmp.getWidth()), (int) (scale * tmp.getHeight()), false);
    }

    @Override
    public Rect getHitBox() {
        if(jumpTime > 0 || recoveryTime > 0 || !hitEnabled || ghostMode) return null;
        return hitBox;
    }

    @Override
    public void setHitEnabled(boolean hitEnabled) {
        if(jumpTimeCool > 0) return;
        if(jumpTime > 0) return;
        if(!hitEnabled && this.hitEnabled){
            currentSprite = jumpSprite;
            animationDelay = ANIMATION_DELAY_JUMP;
            jumpTime = DEFAULT_RUNNER_JUMP_TIME;
            jumpTimeCool = DEFAULT_RUNNER_JUMP_TIME_COOL;
        }
        super.setHitEnabled(hitEnabled);
    }

    public void setGhostMode(boolean ghostMode) {
        this.ghostMode = ghostMode;
        if(ghostMode)
            paint.setAlpha(OPACITY_DEAD);
        else
            paint.setAlpha(255);
    }
}







