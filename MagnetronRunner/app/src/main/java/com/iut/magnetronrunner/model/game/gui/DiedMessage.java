package com.iut.magnetronrunner.model.game.gui;

import com.iut.magnetronrunner.model.game.gameObjects.IDrawable;

public abstract class DiedMessage implements IDrawable {


    protected boolean activated;


    public DiedMessage(boolean activated) {
        this.activated = activated;
    }


    public boolean isActivated() {
        return activated;
    }


    public void setActivated(boolean activated) {
        this.activated = activated;
    }

}
