package com.iut.magnetronrunner.model.game.factory;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.iut.magnetronrunner.model.game.gameObjects.Flame;
import com.iut.magnetronrunner.model.game.hearts.Heart;
import com.iut.magnetronrunner.model.utils.ToolBox;

import java.util.Map;
import java.util.TreeMap;

public class SimpleFactory extends Factory {

    /**
     * Must be static because we don't want to load twice the same assets
     */
    private static Map<Integer,Bitmap> assets;



    public SimpleFactory(Context context){
        super(context);
        assets = new TreeMap<>();
    }



    @Override
    public Flame buildFlame() {
        return buildFlame(ToolBox.getRandomXInTheScreen(context.getResources()));

    }

    @Override
    public Flame buildFlame(int pos) {
        return new Flame(context,pos);
    }

    @Override
    public Heart buildHeart(int pos) {
        return new Heart(context,pos);
    }


    @Override
    public Bitmap loadBitmap(Resources resources, int toLoad) {
        if(assets.containsKey(toLoad)){
            return assets.get(toLoad);
        }
        Bitmap tmp = internLoadBitmap(resources, toLoad);
        assets.put(toLoad,tmp);
        return  tmp;
    }



    private Bitmap internLoadBitmap(Resources resources ,int toLoad) {
        return BitmapFactory.decodeResource(resources, toLoad);
    }


}
