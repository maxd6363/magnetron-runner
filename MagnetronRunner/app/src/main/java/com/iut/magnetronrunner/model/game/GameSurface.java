package com.iut.magnetronrunner.model.game;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;

import androidx.annotation.NonNull;

import com.iut.magnetronrunner.model.game.manager.GameManager;

public class GameSurface extends View {

    private long beforeUpdateNS;
    private long updateTime;
    private long elapsedMS;
    private long sleepTime;
    private long currentNS;
    private boolean running;

    private GameManager gameManager;

    public GameSurface(Context context) {
        super(context);
        currentNS = System.nanoTime();
        running = true;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        beforeUpdateNS = System.nanoTime();
        updateTime = beforeUpdateNS - currentNS;
        elapsedMS = updateTime / 1000000L;

        gameManager.drawGame(canvas);
        gameManager.update(elapsedMS);

        invalidate();

        currentNS = System.nanoTime();
        sleepTime = Math.max(2, 17 - (currentNS - beforeUpdateNS));
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException ignored) {

        }

    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    @NonNull
    public String toString() {
        return "GameSurface{" + "running=" + running + '}';
    }


    public GameManager getGameManager() {
        return gameManager;
    }

    public void setGameManager(GameManager gameManager) {
        this.gameManager = gameManager;
    }







}
