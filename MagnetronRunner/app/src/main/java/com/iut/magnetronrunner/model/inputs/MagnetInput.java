package com.iut.magnetronrunner.model.inputs;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class MagnetInput extends AbstractInput implements SensorEventListener, ISensorInput {

    private final static int SENSOR_MARGIN = 300;

    protected SensorManager sensorManager;
    protected Sensor magneticSensor;
    protected float[] values;
    protected boolean isSensorRegistered;
    protected boolean isSensorAvailable;

    public MagnetInput(Context context) {
        super(context);
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        isSensorRegistered = false;

        isSensorAvailable = true;

        if (magneticSensor == null) {
            isSensorAvailable = false;
        }


    }


    @Override
    public void update(long elapsed) {

        if (isSensorAvailable && isSensorRegistered && values != null) {
            left = values[0] < -SENSOR_MARGIN;
            right = values[0] > SENSOR_MARGIN;
            jump = values[1] > SENSOR_MARGIN;

        } else {
            left = false;
            right = false;
            jump = false;
        }

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        values = event.values;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void startSensor() {
        if (!isSensorRegistered) {
            sensorManager.registerListener(this, magneticSensor, SensorManager.SENSOR_DELAY_FASTEST);
            isSensorRegistered = true;
        }
    }

    @Override
    public void stopSensor() {
        if (isSensorRegistered) {
            sensorManager.unregisterListener(this, magneticSensor);
            isSensorRegistered = false;
        }
    }


}
