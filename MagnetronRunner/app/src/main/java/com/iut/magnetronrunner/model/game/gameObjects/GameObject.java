package com.iut.magnetronrunner.model.game.gameObjects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.iut.magnetronrunner.model.game.factory.Factory;
import com.iut.magnetronrunner.model.utils.IntVector2;
import com.iut.magnetronrunner.model.utils.ToolBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe abstraite représentant tous les objets pouvant se mettre dans le jeu
 * Avec un Sprite et une HitBox (rect) pour les collisions
 */
public abstract class GameObject implements IUpdatable, IDrawable {

    protected final static boolean COLLISION_DEBUG = false;
    private final static float HIT_BOX_SCALE = 1.2f;

    protected Context applicationContext;
    protected Factory factory;
    protected Bitmap[] sprites;
    protected Bitmap currentSprite;
    protected List<Integer> resourceSprites;
    protected int indexCurrentSprite;
    protected Rect rect;


    protected Rect hitBox;
    protected float scale;
    protected IntVector2 position;
    protected Paint paint;
    protected float animationDelay;
    protected float damageInflicted;
    protected float health;
    protected boolean hitEnabled;


    public GameObject(Context context) {
        super();
        applicationContext = context;
        resourceSprites = new ArrayList<>();
        indexCurrentSprite = 0;
        rect = new Rect();
        hitBox = new Rect();
        scale = 1;
        position = new IntVector2();
        paint = new Paint();
        paint.setAntiAlias(false);
        paint.setFilterBitmap(false);
        animationDelay = 0;
        damageInflicted = 0;
        health = 0;
        hitEnabled = true;
    }


    @Override
    public abstract void update(long elapsedMS);

    @Override
    public void draw(Canvas canvas) {
        if (COLLISION_DEBUG)
            canvas.drawRoundRect(new RectF(hitBox), 1, 1, new Paint());
    }

    protected abstract void processPosition();

    protected void processHitBox() {
        hitBox.top = (int) (rect.top - (rect.height() * ( 1 - HIT_BOX_SCALE)));
        hitBox.left = (int) (rect.left - (rect.width() * ( 1 - HIT_BOX_SCALE)));
        hitBox.bottom = (int) (rect.bottom + (rect.height() * ( 1 - HIT_BOX_SCALE)));
        hitBox.right = (int) (rect.right + (rect.width() * ( 1 - HIT_BOX_SCALE)));
    }

    protected abstract void loadSprites();

    public void moveX(float delta) {
        rect.left += delta;
        rect.right += delta;
    }

    public void moveY(float delta) {
        rect.top += delta;
        rect.bottom += delta;
    }

    public Paint getPaint() {
        return paint;
    }

    public Rect getHitBox() {
        return hitEnabled ? hitBox : null;
    }

    public boolean isHitEnabled() {
        return hitEnabled;
    }

    public void setHitEnabled(boolean hitEnabled) {
        this.hitEnabled = hitEnabled;
    }


    public float getDamageInflicted() {
        return damageInflicted;
    }

    public float getHealth() {
        return health;
    }

    @NonNull
    @Override
    public String toString() {
        return "< " + hashCode() + " > GameObject : " + rect;
    }


    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;

        if (obj.getClass() != getClass()) return false;
        return ((GameObject) obj).rect == rect && ((GameObject) obj).position == position && ((GameObject) obj).scale == scale;

    }

    @Override
    public int hashCode() {
        return rect.hashCode() + position.hashCode() + Float.hashCode(scale);
    }
}