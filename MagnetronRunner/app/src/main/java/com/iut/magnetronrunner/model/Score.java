package com.iut.magnetronrunner.model;

public class Score implements Comparable<Score>{

    private String name;
    private float score;
    private int rank;

    public Score(String name, float score,int rank){
        this.score = score;
        this.name = name;
        this.rank = rank;
    }

    public int getRank() {
        return rank;
    }

    public float getScore() {
        return score;
    }

    public String getName() {
        return name;
    }

    public void setRank(int rank){ this.rank = rank; }

    @Override
    public int compareTo(Score score) {
        if(getScore() <= score.getScore())
            return 1;
        return 0;
    }
}
