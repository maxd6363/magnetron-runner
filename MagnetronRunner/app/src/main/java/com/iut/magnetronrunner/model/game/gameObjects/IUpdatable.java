package com.iut.magnetronrunner.model.game.gameObjects;

public interface IUpdatable {
    void update(long elapsed);
}
