package com.iut.magnetronrunner.model;

import android.content.Context;

import androidx.annotation.NonNull;

public abstract class Settings {

    protected Context appContext;

    public Settings(Context appContext) {
        this.appContext = appContext;
    }

    public abstract int getValueMusic();

    public abstract void setValueMusic(int valueMusic);

    public abstract int getValueSound();

    public abstract void setValueSound(int valueSound);

    public abstract int getInput();

    public abstract void setInput(int input);

    public abstract String getId();

    public abstract void setId(String id);

    public abstract String getName();

    public abstract void setName(String id);

    public abstract String getLocale();

    public abstract void setLocale(String locale);



    @NonNull
    @Override
    public String toString() {
        return "Value Music: " + getValueMusic() + " | Value Sound : " + getValueSound();
    }

}
