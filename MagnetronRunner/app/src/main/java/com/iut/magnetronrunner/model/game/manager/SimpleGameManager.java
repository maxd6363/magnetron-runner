package com.iut.magnetronrunner.model.game.manager;

import android.content.Context;
import android.graphics.Canvas;

import com.iut.magnetronrunner.model.game.GameSurface;
import com.iut.magnetronrunner.model.game.collision.SimpleCollisionHandler;
import com.iut.magnetronrunner.model.game.gameObjects.GameObject;
import com.iut.magnetronrunner.model.game.gameObjects.IDrawable;
import com.iut.magnetronrunner.model.game.gameObjects.IUpdatable;
import com.iut.magnetronrunner.model.game.gameObjects.Runner;
import com.iut.magnetronrunner.model.game.gameObjects.background.Background;
import com.iut.magnetronrunner.model.game.gameObjects.background.DynamicNaturalBackground;
import com.iut.magnetronrunner.model.game.generator.SimpleGameObjectGenerator;
import com.iut.magnetronrunner.model.game.gui.SimpleDiedMessage;
import com.iut.magnetronrunner.model.game.hearts.SimpleHeartManager;
import com.iut.magnetronrunner.model.game.player.SimplePlayer;
import com.iut.magnetronrunner.model.game.sound.MusicHandler;
import com.iut.magnetronrunner.model.google.GoogleAuth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class SimpleGameManager extends GameManager {


    private Background background;
    private Collection<GameObject> toDelete;
    private Collection<GameObject> toAdd;
    private Collection<IDrawable> toDeleteGui;
    private Collection<IDrawable> toAddGui;
    private MusicHandler musics;
    private GoogleAuth googleAuth;
    private boolean gameRunning;

    public SimpleGameManager(Context context, GameSurface surface) {
        super(context, surface);

        googleAuth = new GoogleAuth(context);
        musics = new MusicHandler(context);

        createGame();

    }


    private void createGame() {
        gameObjects = new ArrayList<>();
        gameObjectsSecure = Collections.unmodifiableCollection(gameObjects);


        guiElements = new ArrayList<>();
        guiElementsSecure = Collections.unmodifiableCollection(guiElements);

        toDelete = new ArrayList<>();
        toAdd = new ArrayList<>();
        toDeleteGui = new ArrayList<>();
        toAddGui = new ArrayList<>();

        background = new DynamicNaturalBackground(applicationContext);
        collisionHandler = new SimpleCollisionHandler(this);
        player = new SimplePlayer(this);
        heartmanager = new SimpleHeartManager(this);
        gameObjectGenerator = new SimpleGameObjectGenerator(this);
        setRunner(new Runner(applicationContext));

        diedMessage = new SimpleDiedMessage(false, this);
        guiElements.add(diedMessage);
    }

    @Override
    public void update(long elapsedMS) {
        if(!gameRunning) return;
        manageGuiElements();
        manageGameObjects();

        for (IUpdatable updatable : gameObjects) {
            updatable.update(elapsedMS);
        }

        collisionHandler.handleCollision();
        gameObjectGenerator.update(elapsedMS);
        player.update(elapsedMS);
        background.update(elapsedMS);
        heartmanager.update(elapsedMS);

    }


    private void manageGameObjects() {
        if (toDelete.size() > 0) {
            for (GameObject go : toDelete) {
                gameObjects.remove(go);
            }
            toDelete.clear();
        }
        if (toAdd.size() > 0) {
            gameObjects.addAll(toAdd);
            toAdd.clear();
        }
    }

    private void manageGuiElements() {
        if (toDeleteGui.size() > 0) {
            for (IDrawable drawable : toDeleteGui) {
                guiElements.remove(drawable);
            }
            toDeleteGui.clear();
        }
        if (toAddGui.size() > 0) {
            guiElements.addAll(toAddGui);
            toAddGui.clear();
        }
    }

    /**
     * this method won't directly delete the obj, it will be deleted at the next update
     *
     * @param obj to delete
     */
    @Override
    public void deleteGameObject(GameObject obj) {
        toDelete.add(obj);
    }


    /**
     * this method won't directly add the obj, it will be added at the next update
     *
     * @param obj to delete
     */
    @Override
    public void addGameObject(GameObject obj) {
        toAdd.add(obj);
    }

    @Override
    public void deleteGuiElement(IDrawable obj) {
        toDeleteGui.add(obj);
    }

    @Override
    public void addGuiElement(IDrawable obj) {
        toAddGui.add(obj);
    }


    @Override
    public void drawGame(Canvas canvas) {
        if(!gameRunning) return;
        background.draw(canvas);
        for (IDrawable drawable : gameObjects) {
            drawable.draw(canvas);
        }
        for (IDrawable drawable : guiElements) {
            drawable.draw(canvas);
        }
    }


    @Override
    public void playerTookDamage(float damage) {
        player.controlledObjectTookDamage(damage);
        heartmanager.setNumberOfHearts((int) player.getControlledObject().getHealth());
    }

    @Override
    public void startGame() {
        ((SimplePlayer) (player)).startSensors();
        musics.start();
        player.startScore();
        gameRunning = true;
    }

    @Override
    public void stopGame() {
        ((SimplePlayer) (player)).stopSensors();
        musics.stop();
        player.stopScore();
        gameRunning = false;
    }

    @Override
    public void resetGame() {
        createGame();
    }

    @Override
    public void endGame() {
        googleAuth.add_dataBase(player.getScore());
        diedMessage.setActivated(true);
        player.stopScore();
        ((Runner)player.getControlledObject()).setGhostMode(true);
    }


    public Background getBackground() {
        return background;
    }


}
