package com.iut.magnetronrunner.model.game.hearts;

import com.iut.magnetronrunner.model.game.factory.SimpleFactory;
import com.iut.magnetronrunner.model.game.gameObjects.GameObject;
import com.iut.magnetronrunner.model.game.manager.GameManager;

public class SimpleHeartManager extends HeartManager {

    private final static int NUMBER_HEARTS = 3;


    public SimpleHeartManager(GameManager gameManager) {
        super(gameManager);
        factory = new SimpleFactory(gameManager.getApplicationContext());
        for (int i = 1; i <= NUMBER_HEARTS; i++)
            generateHeart(i);
    }

    @Override
    public void setNumberOfHearts(int number) {
        if(number == hearts.size()) return;
        for(GameObject go : hearts)
            gameManager.deleteGameObject(go);
        hearts.clear();

        for (int i = 1; i <= number; i++)
            generateHeart(i);

    }

    @Override
    public void update(long elapsed) {

    }

    @Override
    protected void generateHeart(int pos) {
        Heart heart = factory.buildHeart(pos);
        hearts.add(heart);
        gameManager.addGameObject(heart);
    }
}
