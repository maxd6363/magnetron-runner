package com.iut.magnetronrunner.model.game.gameObjects.background;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;

public abstract class NaturalBackground extends Background {

    public NaturalBackground(Context context) {
        super(context);
    }
}
