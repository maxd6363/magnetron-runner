package com.iut.magnetronrunner.view;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.WindowManager;
import androidx.appcompat.app.AppCompatActivity;
import com.iut.magnetronrunner.model.game.manager.GameManager;
import com.iut.magnetronrunner.model.game.GameSurface;
import com.iut.magnetronrunner.model.game.manager.SimpleGameManager;
import com.iut.magnetronrunner.model.game.player.SimplePlayer;


public class GameActivity extends AppCompatActivity {


    private GameManager gameManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GameSurface gameSurface = new GameSurface(getApplicationContext());
        setContentView(gameSurface);

        gameManager = new SimpleGameManager(getApplicationContext(),gameSurface);
        gameSurface.setGameManager(gameManager);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }


    @Override
    protected void onResume() {
        super.onResume();
        gameManager.startGame();
    }


    @Override
    protected void onPause() {
        super.onPause();
        gameManager.stopGame();
    }
}
