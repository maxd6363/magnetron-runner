package com.iut.magnetronrunner.model.game.factory;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;

import com.iut.magnetronrunner.model.game.gameObjects.Flame;
import com.iut.magnetronrunner.model.game.hearts.Heart;

public abstract class Factory {

    protected Context context;

    public Factory(Context context) {
        this.context = context;
    }

    public abstract Flame buildFlame();

    public abstract Flame buildFlame(int pos);

    public abstract Heart buildHeart(int pos);

    public abstract Bitmap loadBitmap(Resources resources, int toLoad);
}
