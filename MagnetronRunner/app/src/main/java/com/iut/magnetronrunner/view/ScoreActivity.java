package com.iut.magnetronrunner.view;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.Score;
import com.iut.magnetronrunner.model.google.GoogleAuth;

import java.util.ArrayList;

public class ScoreActivity extends AppCompatActivity {


    private DatabaseReference mPostReference;
    private ValueEventListener mPostListener;
    private DatabaseReference mDatabase;
    private ListView lv;
    private ArrayList<Score> users;
    private View load;
    private GoogleAuth googleAuth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.zoom_out, R.anim.zoom_in);

        googleAuth = new GoogleAuth(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        load = findViewById(R.id.load);
        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(500);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());

        load.startAnimation(rotate);
        lv = findViewById(R.id.lv);
        users = googleAuth.get_rank(lv,load,rotate);
    }


}
