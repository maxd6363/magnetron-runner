package com.iut.magnetronrunner.model.game.manager;

import android.content.Context;
import android.graphics.Canvas;

import com.iut.magnetronrunner.model.game.GameSurface;
import com.iut.magnetronrunner.model.game.collision.CollisionHandler;
import com.iut.magnetronrunner.model.game.gameObjects.GameObject;
import com.iut.magnetronrunner.model.game.gameObjects.IDrawable;
import com.iut.magnetronrunner.model.game.gui.DiedMessage;
import com.iut.magnetronrunner.model.game.hearts.HeartManager;
import com.iut.magnetronrunner.model.game.gameObjects.IUpdatable;
import com.iut.magnetronrunner.model.game.gameObjects.Runner;
import com.iut.magnetronrunner.model.game.generator.GameObjectGenerator;
import com.iut.magnetronrunner.model.game.player.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class GameManager implements IUpdatable {

    protected Context applicationContext;
    protected GameSurface gameSurface;
    protected Collection<GameObject> gameObjects;
    protected Collection<GameObject> gameObjectsSecure;
    protected Collection<IDrawable> guiElements;
    protected Collection<IDrawable> guiElementsSecure;

    protected Player player;
    protected HeartManager heartmanager;
    protected CollisionHandler collisionHandler;
    protected GameObjectGenerator gameObjectGenerator;
    protected DiedMessage diedMessage;


    public GameManager(Context context, GameSurface surface) {
        applicationContext = context;
        gameSurface = surface;
    }

    /**
     * please use this methode to set runner instead of accessing the attribut directly
     *
     * @param runner
     */
    protected void setRunner(Runner runner) {
        player.setControlledObject(runner);
        gameObjects.add(runner);
    }

    public GameObject getRunner() {
        return player.getControlledObject();
    }



    public DiedMessage getDiedMessage() {
        return diedMessage;
    }


    public Player getPlayer() {
        return player;
    }

    public abstract void deleteGameObject(GameObject obj);

    public abstract void addGameObject(GameObject obj);

    public abstract void deleteGuiElement(IDrawable obj);

    public abstract void addGuiElement(IDrawable obj);

    @Override
    public abstract void update(long elapsedMS);

    public abstract void drawGame(Canvas canvas);

    public Collection<GameObject> getGameObjects() {
        return gameObjectsSecure;
    }

    public Collection<IDrawable> getGuiElements() {
        return guiElementsSecure;
    }

    public Context getApplicationContext() {
        return applicationContext;
    }

    public abstract void playerTookDamage(float damage);

    public GameSurface getGameSurface() {
        return gameSurface;
    }

    public abstract void startGame();

    public abstract void stopGame();

    public abstract void resetGame();

    public abstract void endGame();
}
