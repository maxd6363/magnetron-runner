package com.iut.magnetronrunner.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.Score;

import java.util.List;

public class ScoreListAdapter extends ArrayAdapter<Score> {

    private static final String TAG = "ScoreListAdapter";

    private Context context;
    private int resource;

    public ScoreListAdapter(@NonNull Context context, int resource, @NonNull List<Score> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String name = getItem(position).getName();
        float score = getItem(position).getScore();
        int rank = getItem(position).getRank();

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resource, parent, false);

        TextView nameScore = convertView.findViewById(R.id.textView);
        TextView scoreScore = convertView.findViewById(R.id.textView2);
        LinearLayout rankScore = convertView.findViewById(R.id.medal);

        nameScore.setText(name);
        scoreScore.setText(String.valueOf(score));
        Log.d("SCORE", String.valueOf(rank));

        switch (rank) {
            case 1:
                rankScore.setBackgroundResource(R.drawable.ic_first);
                break;
            case 2:
                rankScore.setBackgroundResource(R.drawable.ic_second);
                break;
            case 3:
                rankScore.setBackgroundResource(R.drawable.ic_third);
                break;

        }
        return convertView;
    }
}
