package com.iut.magnetronrunner.model.game.collision;

import android.graphics.Rect;

import com.iut.magnetronrunner.model.game.gameObjects.GameObject;
import com.iut.magnetronrunner.model.game.gameObjects.background.DynamicNaturalBackground;
import com.iut.magnetronrunner.model.game.manager.GameManager;
import com.iut.magnetronrunner.model.game.manager.SimpleGameManager;

public class SimpleCollisionHandler extends CollisionHandler {

    private DynamicNaturalBackground background;

    public SimpleCollisionHandler(GameManager manager) {
        super(manager);
        background = (DynamicNaturalBackground) ((SimpleGameManager)manager).getBackground();
    }


    @Override
    public void handleCollision() {
        if (manager.getRunner().getHitBox() == null) return;

        for(Rect r : background.getHitBoxes()){
            Rect rect1 = manager.getRunner().getHitBox();
            if (rect1 == null) break;
            if(Rect.intersects(rect1, r))
                manager.playerTookDamage(background.getDamageInflicted());
        }

        for (GameObject go : gameObjects) {
            if (go.equals(manager.getRunner())) continue;
            if (go.getHitBox() == null) continue;
            Rect rect1 = manager.getRunner().getHitBox();
            if (rect1 == null) continue;
            Rect rect2 = go.getHitBox();
            if (rect2 == null) continue;
            if (Rect.intersects(rect1, rect2)) {
                manager.deleteGameObject(go);
                manager.playerTookDamage(go.getDamageInflicted());
            }
            if (rect2.top > heightScreen + belowMargin) {
                manager.deleteGameObject(go);
            }
        }


    }


}
