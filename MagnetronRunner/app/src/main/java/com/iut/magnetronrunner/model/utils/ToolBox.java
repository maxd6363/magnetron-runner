package com.iut.magnetronrunner.model.utils;

import android.content.res.Resources;
import android.util.TypedValue;

import java.util.Random;

public class ToolBox {

    private static Random random;

    static {
        random = new Random();
    }

    public static long secondToMs(float second) {
        return (long) second * 1000L;
    }

    public static int getHeightDisplay(Resources resource) {
        return resource.getDisplayMetrics().heightPixels;
    }

    public static int getWidthDisplay(Resources resource) {
        return resource.getDisplayMetrics().widthPixels;
    }

    public static int dpToPixel(Resources resource, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resource.getDisplayMetrics());
    }

    public static int getRandomXInTheScreen(Resources resource) {
        return random.nextInt(getWidthDisplay(resource));
    }


    public static boolean getBit(int number, int position) {
        return (((number >> position) & 1) == 1);
    }

}
