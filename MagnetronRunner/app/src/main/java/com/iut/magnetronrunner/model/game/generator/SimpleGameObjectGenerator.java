package com.iut.magnetronrunner.model.game.generator;

import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.game.factory.SimpleFactory;
import com.iut.magnetronrunner.model.game.gameObjects.Flame;
import com.iut.magnetronrunner.model.game.manager.GameManager;

import java.util.Random;

public class SimpleGameObjectGenerator extends GameObjectGenerator {

    private final static float TIME_BETWEEN_FIREBALL = 0.3f;
    private final static float PROBABILITY_TRIPLE_FIREBALL = 0.5f;
    private long fireballTimeBreak;
    private Random random;

    public SimpleGameObjectGenerator(GameManager gameManager) {
        super(gameManager);
        factory = new SimpleFactory(gameManager.getApplicationContext());
        fireballTimeBreak = 0L;
        random = new Random();
    }

    @Override
    protected void generateFlame() {
        if(random.nextDouble() < PROBABILITY_TRIPLE_FIREBALL){
            Flame first = factory.buildFlame();
            Flame second = factory.buildFlame(first.getHitBox().left);
            Flame third = factory.buildFlame(first.getHitBox().right);

            second.setFallSpeed(first.getFallSpeed());
            third.setFallSpeed(first.getFallSpeed());

            gameManager.addGameObject(first);
            gameManager.addGameObject(second);
            gameManager.addGameObject(third);
        }
        else{
            gameManager.addGameObject(factory.buildFlame());
        }
    }

    @Override
    public void update(long elapsed) {
        fireballTimeBreak += elapsed;
        if (fireballTimeBreak > TIME_BETWEEN_FIREBALL * 1000) {
            generateFlame();
            fireballTimeBreak = 0;
        }



    }
}
