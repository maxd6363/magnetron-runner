package com.iut.magnetronrunner.model.game.sound;

import android.content.Context;
import android.media.MediaPlayer;

import com.iut.magnetronrunner.R;
import com.iut.magnetronrunner.model.Settings;
import com.iut.magnetronrunner.model.SharedPreferencesSettings;

public class MusicHandler {

    private Context context;
    private MediaPlayer mediaPlayer;
    private Settings settings;

    public MusicHandler(Context context){
        this.context = context;

        settings = new SharedPreferencesSettings(context);

        mediaPlayer = MediaPlayer.create(context, R.raw.main_theme);
        float volume = settings.getValueMusic() / 100f;
        mediaPlayer.setVolume(volume, volume);

    }


    public void start(){
        mediaPlayer.start();
    }

    public void stop(){
        mediaPlayer.stop();
    }





}
